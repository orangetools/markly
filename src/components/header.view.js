import React from 'react';
import '../assets/css/header.css';

function HeaderView (props) {
    return (
        <div className="Header">
            <h1>{props.title}</h1>
            <h4>{props.subtitle}</h4>
        </div>
    )
}
export default HeaderView;