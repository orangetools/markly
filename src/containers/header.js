import React from 'react';
import HeaderView from '../components/header.view'

class Header extends React.Component {
    render() {
        return (
            <HeaderView title="Markly" subtitle="The Definitive Bookmarks Tool"/>
        )
    }
}

export default Header;