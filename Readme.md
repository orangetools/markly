# Markly
## The Bookmarks App

Markly is a Web App for bookmark all that messy sites you have in everywhere and manage all in just one site. 


## Build

### Development Build
Run `npm run build:dev` and go to `localhost:9000`. 
Browser will autoreload when you make a change and save. 

### Production Build
Run `npm run build`. Webpack will generate a `dist` folder. You can open `dist/index.html` and see the Prod version. 